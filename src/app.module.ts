import { IntrospectAndCompose, RemoteGraphQLDataSource } from '@apollo/gateway';
import { ApolloGatewayDriver, ApolloGatewayDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { decode, verify } from 'jsonwebtoken';

class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  async willSendRequest({ request, context }) {
    const token = context?.req?.headers?.authorization?.split(' ')[1];
    if (token && verify(token, 'secret')) {
      const decoded = decode(token);
      request.http.headers.set('user', JSON.stringify(decoded));
    }
  }
}

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloGatewayDriverConfig>({
      driver: ApolloGatewayDriver,
      server: {
        context: ({ req }) => ({
          jwt: req.headers.authorization,
        }),
        cors: true,
      },
      gateway: {
        buildService: ({ name, url }) => new AuthenticatedDataSource({ url }),
        supergraphSdl: new IntrospectAndCompose({
          subgraphs: [
            { name: 'products', url: 'http://localhost:8002/graphql' },
            { name: 'business', url: 'http://localhost:8003/graphql' },
          ],
        }),
      },
    }),
  ],
  providers: [AuthenticatedDataSource],
})
export class AppModule {}
